#include "../include/transform.h"
#include <stdlib.h>

static inline size_t new_index(size_t i, size_t j,uint64_t height) {
	return height*j + height-i-1;
}
static inline size_t old_index(size_t i, size_t j,uint64_t width) {
	return width*i + j;
}
struct image rotate( struct image const source ) {
    struct image copy_image = image_create(source.height,source.width);
    for (size_t i = 0; i < source.height; i++) {
        for (size_t j = 0; j < source.width; j++) {
          copy_image.data[new_index(i,j,source.height)] = source.data[old_index(i,j,source.width)];
        }
    }
    return copy_image;
}
