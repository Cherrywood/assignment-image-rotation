#include "../include/status.h"
#include <stdio.h>

static const char * const open_mes[]={
        [OPEN_OK] = "Файл открылся успешно",
        [OPEN_INVALID_FILE_NAME] = "Неправильное имя файла",
        [OPEN_ERROR] = "Файл открыть не удалось"
};

static const char * const read_mes[]={
        [READ_OK] = "Операция чтения прошла успешно",
        [READ_INVALID_SIGNATURE] = "Ошибка подписи растрового изображения",
        [READ_INVALID_BITS] = "Недопустимое количество битов при чтении",
        [READ_INVALID_HEADER] = "Недопустимый заголовок при чтении",
        [READ_EBADF] = "В указанном потоке stream невозможно изменять позицию ввода в файле",
};

static const char * const write_mes[]={
        [WRITE_OK] = "Операция записи прошла успешно",
        [WRITE_ERROR] = "Не удолось записать в файл",
        [WRITE_INVALID_BITS] = "Недопустимое количество битов при записи",
        [WRITE_INVALID_HEADER] = "Недопустимый заголовок при записи",
        [WRITE_EBADF] = "В указанном потоке stream невозможно изменять позицию вывода в файле"
};

static const char * const close_mes[]={
        [CLOSE_OK] = "Файл закрылся успешно",
        [CLOSE_ERROR] = "Закрыть файл не удалось"
};

void print_read_status(enum read_status status) {
	fputs(read_mes[status],stderr);
}

void print_write_status(enum write_status status) {
	fputs(write_mes[status],stderr);
}

void print_open_status (enum open_status status) {
	fputs(open_mes[status],stderr);
}
void print_close_status (enum close_status status) {
	fputs(close_mes[status],stderr);
}
