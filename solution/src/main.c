#include "../include/bmp_io.h"
#include "../include/bmp.h"
#include "../include/file_io.h"
#include "../include/transform.h"
#include <stdio.h>

int main(int argc, char** argv) {
    (void) argc;
    (void) argv;

    if (argc != 3) {
        fprintf(stderr,"Недостаточно аргументов");
        return 1;
    }

    FILE* in;
    FILE* out;
    enum open_status o_status = file_open(&in,argv[1],"rb");
    enum read_status r_status;
    enum write_status w_status;

    print_open_status(o_status);
    if (o_status) {
        return 1;
    }

    struct image img = {0};
    r_status = from_bmp(in, &img);
    print_read_status(r_status);
    print_close_status(file_close(in));
    if (r_status) {
        return 1;
    }

    const struct image new_image = rotate(img);
    image_free(&img);


    o_status = file_open(&out,argv[2],"wb");
    if (o_status) {
        print_open_status(o_status);
        return 1;
    }
    w_status = to_bmp(out,&new_image);
    image_free(&new_image);
    print_write_status(w_status);
    print_close_status(file_close(out));
    if (w_status) {
        return 1;
    }  
}
