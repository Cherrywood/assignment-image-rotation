#include "../include/bmp.h"
#include "../include/bmp_io.h"

#define BMP_SIGNATURE 0x4D42
#define BMP_BI_SIZE 40
#define BMP_PLANES 1
#define BMP_BIT_COUNT 24
#define BMP_COMPRESSION 0

static inline size_t calculate_padding (uint32_t width) {
    return 4 - width * sizeof(struct pixel) % 4;
}
struct bmp_header bpm_header_create(uint32_t width, uint32_t height) {
    const uint32_t image_size = height * (width * sizeof(struct pixel) + calculate_padding(width));
    return (struct bmp_header) {
        .bfType = BMP_SIGNATURE,
        .bfileSize = sizeof(struct bmp_header) +  image_size,
        .bfReserved = 0,
        .bOffBits = sizeof(struct bmp_header),
        .biSize = BMP_BI_SIZE,
        .biWidth = width,
        .biHeight = height,
        .biPlanes = BMP_PLANES,
        .biBitCount = BMP_BIT_COUNT,
        .biCompression = BMP_COMPRESSION,
        .biSizeImage = image_size,
        .biXPelsPerMeter = 0,
        .biYPelsPerMeter = 0,
        .biClrUsed = 0,
        .biClrImportant = 0
    };
}
static enum read_status read_header(FILE *in, struct bmp_header* header) {
    if (!fread(header, sizeof(struct bmp_header), 1, in) 
	|| header->bfileSize !=  header->bOffBits + header->biSizeImage
	|| header->biPlanes != BMP_PLANES
	|| header->biBitCount != BMP_BIT_COUNT
	|| header->biCompression != BMP_COMPRESSION)
        	return READ_INVALID_HEADER;
    if (header->bfType != BMP_SIGNATURE)
        	return READ_INVALID_SIGNATURE;
    return READ_OK;
}

static enum read_status read_pixels(FILE* in, struct image* img) {
    const size_t padding = calculate_padding(img->width);
    for (size_t i = 0; i < img->height; ++i) {
        if (fread(&img->data[i * img->width], sizeof(struct pixel), img->width, in) != img->width) {
            image_free(img);
            return READ_INVALID_BITS;
        }
        if(fseek(in, padding, SEEK_CUR)) {
           image_free(img);
           return READ_EBADF;
        }
    }
    return READ_OK;

}
enum read_status from_bmp( FILE* in, struct image* img ) {
    struct bmp_header header = {0};
    const enum read_status status = read_header(in, &header);
    if (status != 0) {
        return status;
    }
    if (fseek(in, header.bOffBits, SEEK_SET)) {
        return READ_EBADF;
    }
    *img = image_create(header.biWidth, header.biHeight);
    return read_pixels(in, img);
}

static enum write_status write_header(FILE *out, struct bmp_header* header) {
    if (!fwrite(header, sizeof(struct bmp_header), 1, out))
        return WRITE_INVALID_HEADER;
    return WRITE_OK;
}

static enum write_status write_pixels(FILE* out, struct image const* img) {
    const size_t padding = calculate_padding(img->width);
    for (size_t i = 0; i < img->height; ++i) {
        if (fwrite(&img->data[i * img->width], sizeof(struct pixel), img->width, out) != img->width) {
            return WRITE_INVALID_BITS;
        }
        if (fseek(out, padding, SEEK_CUR))
            return WRITE_EBADF;
    }
    return WRITE_OK;

}

enum write_status to_bmp( FILE* out, struct image const* img ) {
    struct bmp_header header = bpm_header_create(img->width,img->height);
    const enum write_status status = write_header(out, &header);
    if (status != 0) {
        return status;
    }
    if (fseek(out, header.bOffBits, SEEK_SET)) {
        return WRITE_EBADF;
    }
    return write_pixels(out,img);

}

