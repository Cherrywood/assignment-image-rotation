#include "../include/image.h"
#include <malloc.h>


struct image image_create (uint64_t width, uint64_t height) {
    return (struct image) {
        .width = width,
        .height = height,
        .data = malloc(sizeof(struct pixel) * width * height)
    };
}

void image_free(const struct image *image) {
    free(image->data);
}
