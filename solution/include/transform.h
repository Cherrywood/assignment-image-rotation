//
// Created by tvoi-kotik on 28.12.2021.
//

#ifndef SOLUTION_TRANSFORM_H
#define SOLUTION_TRANSFORM_H

#include "image.h"
struct image rotate( struct image const source );

#endif //SOLUTION_TRANSFORM_H
