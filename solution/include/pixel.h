//
// Created by tvoi-kotik on 28.12.2021.
//

#ifndef SOLUTION_PIXEL_H
#define SOLUTION_PIXEL_H

#include <stdint.h>
struct __attribute__((packed)) pixel { uint8_t b, g, r; };

#endif //SOLUTION_PIXEL_H
