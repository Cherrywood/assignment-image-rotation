//
// Created by tvoi-kotik on 28.12.2021.
//

#ifndef SOLUTION_BMP_IO_H
#define SOLUTION_BMP_IO_H

#include "image.h"
#include "status.h"
#include <stdio.h>

enum read_status from_bmp( FILE* in, struct image* img );
enum write_status to_bmp( FILE* out, struct image const* img );

#endif //SOLUTION_BMP_IO_H
