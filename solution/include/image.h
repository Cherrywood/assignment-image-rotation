//
// Created by tvoi-kotik on 28.12.2021.
//

#ifndef SOLUTION_IMAGE_H
#define SOLUTION_IMAGE_H

#include "pixel.h"
#include <stdint.h>

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image image_create (uint64_t width, uint64_t height);
void image_free(const struct image* image);


#endif //SOLUTION_IMAGE_H
