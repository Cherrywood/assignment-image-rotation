//
// Created by tvoi-kotik on 28.12.2021.
//

#ifndef SOLUTION_FILE_IO_H
#define SOLUTION_FILE_IO_H

#include "status.h"
#include <stdbool.h>
#include <stdio.h>

enum open_status file_open(FILE** file, const char* name, const char* mode);
enum close_status file_close(FILE* file);



#endif //SOLUTION_FILE_IO_H
