//
// Created by tvoi-kotik on 28.12.2021.
//

#ifndef SOLUTION_BMP_H
#define SOLUTION_BMP_H
#include <stdbool.h>
#include  <stdint.h>

struct __attribute__((packed)) bmp_header{
uint16_t bfType;
uint32_t bfileSize;
uint32_t bfReserved;
uint32_t bOffBits;
uint32_t biSize;
uint32_t biWidth;
uint32_t biHeight;
uint16_t biPlanes;
uint16_t biBitCount;
uint32_t biCompression;
uint32_t biSizeImage;
uint32_t biXPelsPerMeter;
uint32_t biYPelsPerMeter;
uint32_t biClrUsed;
uint32_t biClrImportant;
};

struct bmp_header bpm_header_create(uint32_t width, uint32_t height);


#endif //SOLUTION_BMP_H
